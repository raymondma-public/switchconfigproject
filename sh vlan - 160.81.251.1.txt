---------------------------------------------------------------------------------------------
Name            VID  Protocol Addr       Flags                       Proto  Ports  Virtual   
                                                                            Active router
                                                                            /Total
---------------------------------------------------------------------------------------------
CHP             2036 160.36.1.252   /24  -f------mop----------------- IP     1 /1   VR-Default 
DC1             2081 160.81.5.254   /24  -f------mop----------------- IP     1 /1   VR-Default 
Default         4093 ------------------------------------------------ ANY    0 /0   VR-Default 
HKT-ToDC7       751  160.1.1.254    /24  -f------mop----------------- IP     1 /1   VR-Default 
Lo0             4053 160.81.251.1   /32  -fL-----mo------------------ IP     0 /0   VR-Default 
Mgmt            4095 ------------------------------------------------ ANY    0 /1   VR-Mgmt    
V252            252  160.81.252.10  /29  -f------mop----------------- IP     1 /1   VR-Default 
V252_64         2064 160.1.252.66   /29  -f------mop----------------- IP     1 /1   VR-Default 
V252_72         2072 160.1.252.74   /29  -f------mop----------------- IP     1 /1   VR-Default 
V252_80         2080 160.1.252.82   /29  -f------mop----------------- IP     1 /1   VR-Default 
V252_88         2088 160.1.252.90   /29  -f------mop----------------- IP     1 /1   VR-Default 
V252_96         2096 160.81.252.98  /29  -f------mop----------------- IP     1 /1   VR-Default 
V4_252          4052 160.81.4.254   /30  -f------mo-------n---------- IP     1 /1   VR-Default 
---------------------------------------------------------------------------------------------
Flags : (B) BFD Enabled, (c) 802.1ad customer VLAN, (C) EAPS Control VLAN,
        (d) Dynamically created VLAN, (D) VLAN Admin Disabled,
        (e) CES Configured, (E) ESRP Enabled, (f) IP Forwarding Enabled,
        (F) Learning Disabled, (i) ISIS Enabled, (I) Inter-Switch Connection VLAN for MLAG,
        (k) PTP Configured, (l) MPLS Enabled, (L) Loopback Enabled,
        (m) IPmc Forwarding Enabled, (M) Translation Member VLAN or Subscriber VLAN,
        (n) IP Multinetting Enabled, (N) Network Login VLAN, (o) OSPF Enabled,
        (O) Flooding Disabled, (p) PIM Enabled, (P) EAPS protected VLAN,
        (r) RIP Enabled, (R) Sub-VLAN IP Range Configured,
        (s) Sub-VLAN, (S) Super-VLAN, (t) Translation VLAN or Network VLAN,
        (T) Member of STP Domain, (v) VRRP Enabled, (V) VPLS Enabled, (W) VPWS Enabled,
        (Z) OpenFlow Enabled

Total number of VLAN(s) : 13 

