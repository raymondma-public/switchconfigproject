Port Summary
Port  Display         VLAN Name          Port  Link  Speed  Duplex
#     String          (or # VLANs)       State State Actual Actual
==================================================================
1                     HGC-ToDC7           E     A     100   FULL 
2                                         E     R                            
3                     V252_64             E     A     1000  FULL 
4                                         E     R                            
5                     V252_72             E     A     1000  FULL 
6                                         E     R                            
7                                         E     R                            
8                                         E     R                            
9                                         E     R                            
10                                        E     R                            
11                                        E     R                            
12                                        E     R                            
13                                        E     R                            
14                                        E     R                            
15                                        E     R                            
16                                        E     R                            
17                                        E     R                            
18                                        E     R                            
19                    (0003)              E     A     1000  FULL 
20                    (0003)              E     R                            
21                    (0003)              E     R                            
22                    V252_56             E     A     1000  FULL 
23                    (0005)              E     A     1000  FULL 
24                    (0005)              E     A     1000  FULL 
25                    V4_1                E     A     100   FULL 
26                    V4_9                E     A     100   FULL 
27                                        E     R                            
28                                        E     R                            
29                                        E     NP                           
30                                        E     NP                           
31                                        E     NP                           
32                                        E     NP                           
33                                        E     NP                           
34                                        E     NP                           
==================================================================
   Port State: D-Disabled, E-Enabled
   Link State: A-Active, R-Ready, NP-Port not present, L-Loopback,
               D-ELSM enabled but not up
               d-Ethernet OAM enabled but not up


