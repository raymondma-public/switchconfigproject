Port Summary
Port  Display         VLAN Name          Port  Link  Speed  Duplex
#     String          (or # VLANs)       State State Actual Actual
==================================================================
1                     V252_160            E     A     1000  FULL 
2                                         E     R                            
3                     (0002)              E     A     1000  FULL 
4                     V43                 E     A     1000  FULL 
5                     V43                 E     A     1000  FULL 
6                     (0002)              E     R                            
7                     (0002)              E     R                            
8                     (0002)              E     A     1000  FULL 
9                     (0002)              E     A     1000  FULL 
10                    (0002)              E     A     1000  FULL 
11                    (0002)              E     A     1000  FULL 
12                    (0002)              E     R                            
13                    (0002)              E     R                            
14                    (0002)              E     R                            
15                    (0002)              E     R                            
16                                        E     R                            
17                                        E     R                            
18                    V171                E     R                            
19                    V171                E     R                            
20                    V171                E     A     1000  FULL 
21                    V171                E     R                            
22                                        E     R                            
23                    (0005)              E     A     1000  FULL 
24                    (0005)              E     A     1000  FULL 
25                                        E     R                            
26                                        E     R                            
27                                        E     R                            
28                                        E     R                            
29                                        E     NP                           
30                                        E     NP                           
==================================================================
   Port State: D-Disabled, E-Enabled
   Link State: A-Active, R-Ready, NP-Port not present, L-Loopback,
               D-ELSM enabled but not up
               d-Ethernet OAM enabled but not up


