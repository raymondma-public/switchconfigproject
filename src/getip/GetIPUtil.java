package getip;

import java.util.ArrayList;
import java.util.Scanner;

import model.IPMapping;


/*
 * This class is for getting wanted ip from command
 */
public class GetIPUtil {

	public static ArrayList<IPMapping> getIPMapFromConfig(String configString) {

		String lo0Ip = getLoop0IP(configString);
		Scanner scanner = new Scanner(configString);
		ArrayList<IPMapping> ipMappingList = new ArrayList();
		while (scanner.hasNextLine()) {
			String currentLine = scanner.nextLine();
			//split the command first
			String[] currentLineSplit = currentLine.split(" ");
			
			//command eg.configure vlan V252_112 ipaddress 160.30.252.114 255.255.255.248
			//currentLineSplit[2] is vlan name
			//currentLineSplit[4] is ip address
			//currentLineSplit[5] is subnet mask
			if (isConfigIpCommand(currentLineSplit)) {

				ipMappingList.add(new IPMapping(lo0Ip, currentLineSplit[2], currentLineSplit[4], currentLineSplit[5]));

			}
		}
		for (IPMapping ipMap : ipMappingList) {
			System.out.println(ipMap.getLoopbackIp() + " " + ipMap.getVlanName() + " " + ipMap.getIp() + " "
					+ ipMap.getSubnetMask());
		}

		return ipMappingList;
	}

	private static boolean isConfigIpCommand(String[] commandSplit) {
		//command eg.configure vlan V252_112 ipaddress 160.30.252.114 255.255.255.248
		return commandSplit.length == 6 && commandSplit[0].equals("configure") && commandSplit[1].equals("vlan")
				&& commandSplit[3].equals("ipaddress");
	}

	/*
	 * This method is for getting the loopback address
	 */
	public static String getLoop0IP(String configString) {
		Scanner scanner = new Scanner(configString);
		
		//loop for all config.
		while (scanner.hasNextLine()) {
			String currentLine = scanner.nextLine();
			String[] currentLineSplit = currentLine.split(" ");
			
			//find the address for the Lo0 vlan, finish
			if (isConfigIpCommand(currentLineSplit) && currentLineSplit[2].equalsIgnoreCase("Lo0")) {
				return currentLineSplit[4];
			}
		}
		return "";
	}

}
