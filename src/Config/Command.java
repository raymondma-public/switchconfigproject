package Config;

/*
 * This class is for storing the switch command as string 
 */
public class Command {
	public static final String CMD_SHOW_PORT_CONFIG_N="sh port config n";//get port config
	public static final String CMD_SHOW_SHARING="sh sharing";//show sharing
	public static final String CMD_SHOW_ESRP="sh esrp";//sh esrp
	public static final String CMD_SHOW_HKT="sh hkt";//sh specific vlan(seldom use)
	public static final String CMD_SHOW_HGC="sh hgc";//sh specific vlan(seldom use) 
	private static Command instance=new Command();
	
	private Command(){
		
	}
	public static Command getInstance(){
		return instance;	
	}
	
	
}
