package exec;

/* -*-mode:java; c-basic-offset:2; indent-tabs-mode:nil -*- */
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.UIKeyboardInteractive;
import com.jcraft.jsch.UserInfo;

import loader.IpsLoader;
import model.Port;
import model.Switch;
import model.User;
import model.UserPreference;
import util.FileAndFolderTool;
import util.HospitalUtil;

public class Exec {

	private static boolean needCheckEwanACL = false;
	private static boolean needCheckTrunkACL = false;

	private static String vlan = "";
	// private static String username = "";
	// private static String password = "";

	private static boolean showAllFrames = false;

	private static User currentUser;

	public static void main(String[] arg) {

		// getInputAndOpenFiles();

		// Create the panel

		//
		FileInputStream fin = null;

		boolean accountFound = false;
		User userFromFile = null;
		ObjectInputStream ois =null;
		try {
			fin = new FileInputStream("data.ser");

			 ois = new ObjectInputStream(fin);
			userFromFile = (User) ois.readObject();
			// JOptionPane.showMessageDialog(null, userFromFile.getUsername());
			// JOptionPane.showMessageDialog(null, userFromFile.getPassword());
			currentUser = userFromFile;
			accountFound = true;
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block

			e1.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {

			if(ois!=null){
				try {
					ois.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if (fin != null) {
				try {
					fin.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}

		JPanel panel = new JPanel();
		GridLayout experimentLayout = new GridLayout(0, 2);
		panel.setLayout(experimentLayout);
		JLabel usernameLabel = new JLabel("Enter a User Name:");
		JTextField userNameField = new JTextField(5);
		userNameField.setBounds(0, 0, 50, 50);
		JLabel label = new JLabel("Enter a password:");
		JPasswordField pass = new JPasswordField(5);

		JLabel storePwLabel = new JLabel("Store Password");
		JCheckBox pwCB = new JCheckBox();

		JLabel showAllFrameLabel = new JLabel("Show All Frames");
		JCheckBox showAllFrameLabelCB = new JCheckBox();

		panel.add(usernameLabel);
		panel.add(userNameField);

		panel.add(label);
		panel.add(pass);

		panel.add(storePwLabel);
		panel.add(pwCB);

		panel.add(showAllFrameLabel);
		panel.add(showAllFrameLabelCB);

		if (accountFound) {
			userNameField.setText(userFromFile.getUsername());
			pass.setText(userFromFile.getPassword());
			showAllFrameLabelCB.setSelected(userFromFile.isShowAllFrame());
			pwCB.setSelected(userFromFile.isStoreUserPassword());
		}

		// Show the panel
		String[] options = new String[] { "OK", "Cancel" };
		int option = JOptionPane.showOptionDialog(null, panel, "The title", JOptionPane.NO_OPTION,
				JOptionPane.PLAIN_MESSAGE, null, options, options[1]);

		if (option == 0) // pressing OK button
		{

			char[] passwordCharArr = pass.getPassword();
			String password = new String(passwordCharArr);
			String username = userNameField.getText();
			UserPreference tempPreference = new UserPreference(showAllFrameLabelCB.isSelected(), pwCB.isSelected());
			currentUser = new User(username, password, tempPreference);

			if (pwCB.isSelected()) {
				FileOutputStream fout=null;
				ObjectOutputStream oos=null;
				try {
					 fout = new FileOutputStream("data.ser");
					 oos = new ObjectOutputStream(fout);

					oos.writeObject(currentUser);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}finally{
					if(fout!=null){
						try {
							fout.close();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					
					if(oos!=null){
						try {
							oos.close();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			} else {

				Path path = Paths.get("./data.ser");

				try {
					Files.delete(path);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					System.out.println("Delete operation is failed.");
					e.printStackTrace();
				}

			}

		} else {
			return;
		}

		try {
			JSch jsch = new JSch();

			String host = null;
			ArrayList<String> switchIps = new ArrayList();

			// ===========Start Getting ip
			System.out.print("IPs:");
			String ips = JOptionPane.showInputDialog(null, "Enter ips(Empty if use ip.txt");
			if (ips.contains("&")) { // add N switch with last octect different,
										// seperated last octect with "&"
										// eg. 192.168.1.1&2
				IpsLoader.addNSwitchWithDiffLastOct(switchIps, ips);
			} else if (ips.contains("|")) { // add N switch with whole ip
											// eg.
											// 192.168.1.1|192.168.2.1|192.168.3.1
				addNSwitchWithWholeIp(switchIps, ips);
			} else if (ips.equals("")) { // add N switches from file with whole
											// ip
				addSwitchesFromFile(switchIps);
			} else {
				switchIps.add(ips);
			}
			// ===========Finish Getting ip

			ArrayList<Switch> switches = new ArrayList();
			ArrayList<Thread> threads = new ArrayList();
			ArrayList<String> commands = new ArrayList();

			// read commands from files
			Scanner commandReader = new Scanner(new File("commands.txt"));
			while (commandReader.hasNextLine()) {
				commands.add(commandReader.nextLine());
			}
			commandReader.close();

			// For all switches, run all commands
			for (String ip : switchIps) {
				createThreadFor1Switch(jsch, currentUser.getUsername(), switches, threads, ip, commands);
			} // for every switch

			System.out.println("Finish getting");

			// Wait all threads to complete
			for (Thread t : threads) {
				t.join();
			}

			// Program finished
			JOptionPane.showMessageDialog(null, "Finished");
		} catch (Exception e) {
			System.out.println(e);
		}

	}

	/*
	 * This create thread and start thread for 1 switch
	 */
	private static void createAndStartUIThread1Switch(ArrayList<Thread> uiThreads, Switch oneSwitch) {
		Thread currentThread = new Thread() {

			public void run() {
				String outputString = "";
				outputString += oneSwitch.getIp() + "\n";
				System.out.println(oneSwitch.getIp());
				for (Port port : oneSwitch.getPorts()) {
					System.out.print(port.getPortNumber() + "\t");
					outputString += port.getPortNumber() + "\t";

					if (port.getStatus().contains("R")) {
						System.out.print("    ");
						outputString += "        ";
					}
					System.out.println(port.getStatus());
					outputString += port.getStatus() + "\n";
				}

				if (currentUser.isShowAllFrame()) {
					JTextArea textArea = new JTextArea(outputString);
					JScrollPane scrollPane = new JScrollPane(textArea);
					textArea.setLineWrap(true);
					textArea.setWrapStyleWord(true);
					scrollPane.setPreferredSize(new Dimension(500, 800));

					JFrame demo = new JFrame(oneSwitch.getIp());
					demo.setSize(500, 800);
					demo.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

					demo.getContentPane().add(BorderLayout.CENTER, scrollPane);

					demo.setVisible(true);
				}
			}
		};
		uiThreads.add(currentThread);
		currentThread.start();
	}

	/*
	 * This create thread for 1 switch
	 */
	private static void createThreadFor1Switch(JSch jsch, String user, ArrayList<Switch> switches,
			ArrayList<Thread> threads, String ip, ArrayList<String> commands) {
		Thread currentThread = new Thread() {

			public void run() {

				try {

					if (needCheckTrunkACL) {
						System.out.println("IP:" + ip);
						Switch currentSwitch = new Switch(ip);
						Session session = jsch.getSession(user, ip, 22);

						UserInfo ui = new MyUserInfo();
						session.setUserInfo(ui);

						session.connect();
						System.out.println("connect");
						ArrayList<String> preCommands = new ArrayList();
						preCommands.add("sh edp port all");

						for (String command : preCommands) {
							String allString = openChannelRunOneCommand(session, command);
							ArrayList<String> trunkPorts = checkTrunkCommand(ip, allString, session, command);
							for (String p : trunkPorts) {

							}
						}
						session.disconnect();
					}

					runAndInitOneSwitch(jsch, user, switches, ip, commands);

				} catch (JSchException | IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		};
		currentThread.start();
		threads.add(currentThread);
	}

	/*
	 * It split the ips string(eg. 192.168.0.1,192.168.0.2) to multiple ip and
	 * add to the list
	 */
	private static void addNSwitchWithWholeIp(ArrayList<String> switchIps, String ips) {
		System.out.println(ips);
		ips = ips.replaceAll(" ", "");

		String allIps[] = ips.split(",");
		for (String ip : allIps) {
			System.out.println(ip);
			// switchIps.add(ip);
		}

		for (String ip : allIps) {
			// System.out.println(ip);
			switchIps.add(ip);
		}
	}

	/*
	 * Reading ips from the file and add to the switch ip list
	 */
	private static void addSwitchesFromFile(ArrayList<String> switchIps) throws FileNotFoundException {
		Scanner fileScanner = new Scanner(new File("ips.txt"));
		while (fileScanner.hasNextLine()) {
			switchIps.add(fileScanner.nextLine());
		}
	}

	/*
	 * This help opening design files
	 */
	private static void getInputAndOpenFiles() {
		String designNumber = JOptionPane.showInputDialog(null, "Design Number").trim();
		String designNumberSplit[] = designNumber.split("_");
		String hostpitalFileCode = designNumberSplit[0];
		String hostpiralFolderCode = HospitalUtil.getInstance().designFileCodeToFolderCode(hostpitalFileCode);
		// String filePath="\\\\itip.home\\DavWWWRoot\\N3\\Network
		// Designs\\PYN\\PYNEH_0242_1.xlsx";
		String filePath = "\\\\itip.home\\DavWWWRoot\\N3\\Network Designs\\Draft Design\\" + designNumber + ".xlsx";

		Desktop dt = Desktop.getDesktop();

		String portTablePath = "\\\\corp.ha.org.hk\\HA\\HO\\FD\\IT\\NMS\\N3\\Network Implementation\\Network Implementation\\Building Switch Ports\\L3 To L2 switch port assignment (Available).xlsx";

		int needPortTable = JOptionPane.showConfirmDialog(null, "Need Open Port Table?");
		if (needPortTable == JOptionPane.YES_OPTION) {
			try {
				dt.open(new File(portTablePath));
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

		try {
			dt.open(new File(filePath));
		} catch (IOException e1) {

			e1.printStackTrace();
		} catch (IllegalArgumentException e1) {
			filePath = "\\\\itip.home\\DavWWWRoot\\N3\\Network Designs\\" + hostpiralFolderCode + "\\" + designNumber
					+ ".xlsx";
			try {
				dt.open(new File(filePath));
			} catch (IOException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			} catch (IllegalArgumentException e2) {

				filePath = "\\\\itip.home\\DavWWWRoot\\N3\\Network Designs\\GOPCs\\" + hostpiralFolderCode + "\\"
						+ designNumber + ".xlsx";
				try {
					dt.open(new File(filePath));
				} catch (IOException e3) {
					// TODO Auto-generated catch block
					e3.printStackTrace();
				} catch (IllegalArgumentException e4) {

					e4.printStackTrace();
				}

				e2.printStackTrace();
			}
			e1.printStackTrace();
		}
	}

	/*
	 * This run and initialize one switch model status eg. port status
	 * 
	 */
	private static void runAndInitOneSwitch(JSch jsch, String user, ArrayList<Switch> switches, String ip,
			ArrayList<String> commands) throws JSchException, IOException {
		System.out.println("IP:" + ip);
		Switch currentSwitch = new Switch(ip);
		Session session = jsch.getSession(user, ip, 22);
		// commands.add("sh access-list port 6:47");

		/*
		 * String xhost="127.0.0.1"; int xport=0; String
		 * display=JOptionPane.showInputDialog("Enter display name",
		 * xhost+":"+xport); xhost=display.substring(0, display.indexOf(':'));
		 * xport=Integer.parseInt(display.substring(display.indexOf(':') +1)) ;
		 * session.setX11Host(xhost); session.setX11Port(xport+6000);
		 */

		// username and password will be given via UserInfo interface.
		UserInfo ui = new MyUserInfo();
		session.setUserInfo(ui);

		// commands.add("sh port n");
		// commands.add("sh port n");

		session.connect();
		System.out.println("connect");
		ArrayList<String> newCommands = new ArrayList();
		// ArrayList<Thread> threads = new ArrayList();

		for (int i = 0; i < commands.size(); i++) {
			String command = commands.get(i);
			System.out.println("=============Run Command: " + command);
			String allString = openChannelRunOneCommand(session, command);

			// if (command.contains("show access-list port")) {
			//
			// String returnString = "";
			// System.out.println();
			// Scanner scanner = new Scanner(allString);
			// while (scanner.hasNextLine()) {
			// String currentLine = scanner.nextLine();
			// System.out.println(currentLine);
			// returnString += currentLine + "\n";
			// }
			// scanner.close();
			// String outputString = "";
			// outputString += "ip:\t" + ip + "\n";
			// outputString += "port: " + command.replace("show access-list
			// port", "") + "\t numberOfLine:\t"
			// + countLines(returnString) + "\n" + returnString;
			//
			// // if (countLines(returnString) != 0) {
			// JTextArea textArea = new JTextArea(outputString);
			// JScrollPane scrollPane = new JScrollPane(textArea);
			// textArea.setLineWrap(true);
			// textArea.setWrapStyleWord(true);
			// scrollPane.setPreferredSize(new Dimension(500, 100));
			//
			// JFrame demo = new JFrame(ip);
			// demo.setSize(500, 100);
			// demo.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			//
			// demo.getContentPane().add(BorderLayout.CENTER, scrollPane);
			// Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
			// demo.setLocation(dim.width - demo.getSize().width, 0);
			// demo.setVisible(true);
			// // }
			// } else {

			String outputString = "";
			System.out.println();
			Scanner scanner = new Scanner(allString);

			DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd");
			// get current date time with Date()
			Date date = new Date();
			String folderName = dateFormat.format(date);
			File theDir = new File(folderName);

			// if the directory does not exist, create it
			if (!theDir.exists()) {
				System.out.println("creating directory: " + folderName);
				boolean result = false;

				try {
					theDir.mkdir();
					result = true;
				} catch (SecurityException se) {
					// handle it
				}
				if (result) {
					System.out.println("DIR created");
				}
			}

			try {

				while (scanner.hasNextLine()) {
					String currentLine = scanner.nextLine();
					System.out.println(currentLine);
					outputString += currentLine + "\n";

				}

			} catch (Exception e) {
				// do something
			}
			scanner.close();

			FileAndFolderTool.createAndWriteFile(currentSwitch.getIp(), command, outputString);
			if (currentUser.isShowAllFrame()) {
				JTextArea textArea = new JTextArea(outputString);
				JScrollPane scrollPane = new JScrollPane(textArea);
				textArea.setLineWrap(true);
				textArea.setWrapStyleWord(true);
				scrollPane.setPreferredSize(new Dimension(500, 800));

				JFrame demo = new JFrame(currentSwitch.getIp() + " - " + command);
				demo.setSize(500, 800);
				demo.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

				demo.getContentPane().add(BorderLayout.CENTER, scrollPane);
				Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
				demo.setLocation(dim.width / 2, dim.height / 2 - demo.getSize().height / 2);// -
																							// demo.getSize().width
																							// /
																							// 2
				demo.setVisible(true);
			}

			// }//end else

		} // for every command

		session.disconnect();

		switches.add(currentSwitch);
	}

	/*
	 * This handle the sharing command
	 */
	private static void shSharingCommand(Switch currentSwitch, String allString) {
		boolean startedRecording = false;
		int lineCount = -1;
		int seperatorCount = 0;
		Scanner scanner = new Scanner(allString);

		String sharingReport = "";
		String okString = "";
		String notOkString = "";
		String exceptionCase = "";

		while (scanner.hasNextLine()) {
			String currentLine = scanner.nextLine();
			sharingReport += currentLine + "\n";
			if (currentLine
					.contains("Master    Master     Control   Algorithm   Group     Mbr   State   Transitions")) {
				startedRecording = true;
				lineCount = -1;
			}
			if (currentLine.contains("============================================================================")) {
				seperatorCount++;
				if (seperatorCount == 2) {
					break;
				}
			}

			System.out.println(currentLine);

			if (startedRecording && lineCount >= 1) {
				Scanner lineScanner = new Scanner(currentLine);
				// sharingReport+=
				String sharingPort = lineScanner.next().trim();
				// System.out.println("sharingPort: "+sharingPort);
				if (Character.isDigit(sharingPort.charAt(0))) {
					if (currentSwitch.containEsrpPort(sharingPort)) {
						// JOptionPane.showMessageDialog(null, sharingPort);
						okString += sharingPort + " , ";
						currentSwitch.addSharingPortsWithEsrp(sharingPort);
					} else {
						notOkString += sharingPort + " , ";
						currentSwitch.addSharingPortsWithoutEsrp(sharingPort);
						// JOptionPane.showMessageDialog(null, "NO!!!"+
						// sharingPort);
					}
				} else {
					exceptionCase += sharingPort + " , ";
				}

				lineScanner.close();

			}
			lineCount++;
		}

		JTextArea textArea = new JTextArea(sharingReport + "\nExceptions: " + exceptionCase + "\nOK: " + okString
				+ "\tNot OK: " + notOkString + "\t#Esrp record:" + currentSwitch.getEsrpRecordsNumber());
		JScrollPane scrollPane = new JScrollPane(textArea);
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		scrollPane.setPreferredSize(new Dimension(500, 800));

		JFrame demo = new JFrame(currentSwitch.getIp());
		demo.setSize(500, 800);
		demo.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		demo.getContentPane().add(BorderLayout.CENTER, scrollPane);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		demo.setLocation(dim.width / 2, dim.height / 2 - demo.getSize().height / 2);// -
																					// demo.getSize().width
																					// /
																					// 2
		demo.setVisible(true);

		scanner.close();

	}

	/*
	 * This handle the esrp command
	 * 
	 */
	private static void shEsrpCommand(Switch currentSwitch, String allString) {
		Scanner scanner = new Scanner(allString);
		boolean startedRecording = false;
		int lineCount = -1;
		while (scanner.hasNextLine()) {

			String currentLine = scanner.nextLine();
			System.out.println(currentLine);

			if (currentLine.contains("Port	  Weight	Host	Restart")) {
				startedRecording = true;

			}

			String datas[] = currentLine.split(" ");

			if (lineCount >= 1) {
				currentSwitch.addEsrpPort(datas[0]);
				System.out.println(datas[0]);
			}
			if (startedRecording) {
				lineCount++;
			}
		}
		scanner.close();
	}

	/*
	 * This check the ewan
	 */
	private static ArrayList<Port> checkEwanCommand(String ip, String allString, Session session, String command) {
		boolean startRecording = false;
		String untagPortStrings = "";
		Scanner scanner = new Scanner(allString);
		while (scanner.hasNextLine()) {
			String currentLine = scanner.nextLine();
			System.out.println(currentLine);

			if (currentLine.contains("Untag:")) {
				startRecording = true;
			}
			if (currentLine.contains("Tag:")) {
				startRecording = false;
				break;
			}
			if (currentLine.contains("Flags:")) {
				startRecording = false;
				break;
			}
			if (startRecording == true) {
				untagPortStrings += currentLine;
			}

		}

		System.out.println("untagPortStrings: " + untagPortStrings);

		untagPortStrings = untagPortStrings.replaceAll("Untag:", "");
		untagPortStrings = untagPortStrings.trim();
		String allUntagPortsForThisSwitch[] = untagPortStrings.split(",");

		ArrayList<Port> ewanPorts = new ArrayList();
		String portsCanBeUsed = "";
		for (String portString : allUntagPortsForThisSwitch) {
			if (portString.contains("*")) {
				portsCanBeUsed += portString + ", ";
				ewanPorts.add(new Port(portString.replace("*", ""), "A"));
			}
		}

		for (Port p : ewanPorts) {
			try {
				String resultString = openChannelRunOneCommand(session, "show access-list port " + p.getPortNumber());
				// JOptionPane.showMessageDialog(null, ip+" #line:
				// "+countLines(resultString));
			} catch (JSchException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		// JOptionPane.showMessageDialog(null, portsCanBeUsed);

		JTextArea textArea = new JTextArea(portsCanBeUsed);
		JScrollPane scrollPane = new JScrollPane(textArea);
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		scrollPane.setPreferredSize(new Dimension(500, 100));
		// JOptionPane.showMessageDialog(null, scrollPane,
		// "dialog test with textarea",
		// JOptionPane.YES_NO_OPTION);

		JFrame demo = new JFrame(ip);
		demo.setSize(500, 100);
		demo.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		demo.getContentPane().add(BorderLayout.CENTER, scrollPane);

		demo.setVisible(true);

		return ewanPorts;
	}

	/*
	 * This check the trunk
	 * 
	 */
	private static ArrayList<String> checkTrunkCommand(String ip, String allString, Session session, String command) {
		boolean startedRecording = false;
		int lineCount = -1;
		int seperatorCount = 0;
		Scanner scanner = new Scanner(allString);

		String sharingReport = "";
		String okString = "";
		String notOkString = "";
		String exceptionCase = "";
		ArrayList<String> resultPorts = new ArrayList();

		while (scanner.hasNextLine()) {
			String currentLine = scanner.nextLine();
			sharingReport += currentLine + "\n";
			if (currentLine.contains("                                                           Port         Vlans")) {
				startedRecording = true;
				lineCount = -1;
			}
			if (currentLine.contains("============================================================================")) {
				seperatorCount++;
				if (seperatorCount == 2) {
					break;
				}
			}

			System.out.println(currentLine);

			if (startedRecording && lineCount >= 1) {
				Scanner lineScanner = new Scanner(currentLine);
				// sharingReport+=
				String port = lineScanner.next().trim();
				resultPorts.add(port);

				lineScanner.close();

			}
			lineCount++;
		}

		String ports = "";
		for (String s : resultPorts) {
			try {
				String returnString = "";
				String resultString = openChannelRunOneCommand(session, "show access-list port " + s);

				System.out.println();
				Scanner scannerForAccessListResult = new Scanner(resultString);
				while (scannerForAccessListResult.hasNextLine()) {
					String currentLine = scannerForAccessListResult.nextLine();
					System.out.println(currentLine);
					returnString += currentLine + "\n";
				}
				scannerForAccessListResult.close();
				String outputString = "";
				outputString += "ip:\t" + ip + "\n";
				outputString += "port: " + s + "\t numberOfLine:\t" + countLines(returnString) + "\n" + returnString;

				if (countLines(returnString) != 0) {
					JTextArea textArea = new JTextArea(outputString);
					JScrollPane scrollPane = new JScrollPane(textArea);
					textArea.setLineWrap(true);
					textArea.setWrapStyleWord(true);
					scrollPane.setPreferredSize(new Dimension(500, 100));

					JFrame demo = new JFrame(ip);
					demo.setSize(500, 100);
					demo.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

					demo.getContentPane().add(BorderLayout.CENTER, scrollPane);
					Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
					demo.setLocation(dim.width - demo.getSize().width, 0);
					demo.setVisible(true);
				}
			} catch (JSchException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			ports += s + " , ";
		}

		return resultPorts;
	}

	private static void showVlanCommand(String ip, String allString) {
		boolean startRecording = false;
		String untagPortStrings = "";
		Scanner scanner = new Scanner(allString);
		while (scanner.hasNextLine()) {
			String currentLine = scanner.nextLine();
			System.out.println(currentLine);

			if (currentLine.contains("Untag:")) {
				startRecording = true;
			}
			if (currentLine.contains("Tag:")) {
				startRecording = false;
				break;
			}
			if (startRecording == true) {
				untagPortStrings += currentLine;
			}

		}

		System.out.println("untagPortStrings: " + untagPortStrings);

		untagPortStrings = untagPortStrings.replaceAll("Untag:", "");
		untagPortStrings = untagPortStrings.trim();
		String allUntagPortsForThisSwitch[] = untagPortStrings.split(",");

		String portsCanBeUsed = "";
		for (String portString : allUntagPortsForThisSwitch) {
			if (!portString.contains("*")) {
				portsCanBeUsed += portString + ", ";

			}
		}

		JTextArea textArea = new JTextArea(portsCanBeUsed);
		JScrollPane scrollPane = new JScrollPane(textArea);
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		scrollPane.setPreferredSize(new Dimension(500, 100));

		JFrame demo = new JFrame(ip);
		demo.setSize(500, 100);
		demo.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		demo.getContentPane().add(BorderLayout.CENTER, scrollPane);

		demo.setVisible(true);
	}

	/*
	 * Initialize one switch status eg. port status
	 */
	private static void initOneSwitchStatus(Switch currentSwitch, String allString) {
		int portNumber = 0;
		boolean startRecord = false;
		int lineCount = 0;

		Scanner scanner = new Scanner(allString);
		while (scanner.hasNextLine()) {
			String currentLine = scanner.nextLine();
			if (currentLine
					.contains("         router     State State Neg  Cfg Actual Cfg Actual Cntrl Master Pri Red")) {
				portNumber = -1;
				startRecord = true;
			}
			if (currentLine
					.contains("==============================================================================")) {
				lineCount++;
			}

			if (startRecord) {
				if (lineCount == 2) {
					break;
				}

				if (portNumber > 0) {
					System.out.println(currentLine);
					Scanner lineScanner = new Scanner(currentLine);
					String portName = lineScanner.next();
					lineScanner.next();// useless parameter
					lineScanner.next();// useless parameter
					String portStatus = lineScanner.next();
					currentSwitch.addPort(portName, portStatus);
				}
				portNumber++;
			}

		}
		scanner.close();
	}

	/*
	 * Input: session, command Output: it run the command on switch and return
	 * the report as string
	 */
	private static String openChannelRunOneCommand(Session session, String command) throws JSchException, IOException {

		Channel channel = session.openChannel("exec");
		((ChannelExec) channel).setCommand(command);

		channel.setInputStream(null);

		((ChannelExec) channel).setErrStream(System.err);

		InputStream in = channel.getInputStream();

		channel.connect();

		String allString = "";
		byte[] tmp = new byte[1024];
		while (true) {
			while (in.available() > 0) {
				int i = in.read(tmp, 0, 1024);
				if (i < 0)
					break;
				// System.out.print(new String(tmp, 0, i));
				allString += new String(tmp, 0, i);
			}
			if (channel.isClosed()) {
				if (in.available() > 0)
					continue;
				// System.out.println("exit-status: " +
				// channel.getExitStatus());
				break;
			}
			try {
				Thread.sleep(1000);
			} catch (Exception ee) {
			}
		}

		channel.disconnect();
		return allString;
	}

	/*
	 * This class is the user information eg. user name eg. password
	 */
	public static class MyUserInfo implements UserInfo, UIKeyboardInteractive {
		public String getPassword() {
			return passwd;
		}

		public boolean promptYesNo(String str) {
			// Object[] options = { "yes", "no" };
			// int foo = JOptionPane.showOptionDialog(null, str, "Warning",
			// JOptionPane.DEFAULT_OPTION,
			// JOptionPane.WARNING_MESSAGE, null, options, options[0]);
			// return foo == 0;
			return true;
		}

		String passwd;
		JTextField passwordField = (JTextField) new JPasswordField(20);

		public String getPassphrase() {
			return null;
		}

		public boolean promptPassphrase(String message) {
			return true;
		}

		public boolean promptPassword(String message) {
			passwd = currentUser.getPassword();
			return true;
			// Object[] ob={passwordField};
			// int result=
			// JOptionPane.showConfirmDialog(null, ob, message,
			// JOptionPane.OK_CANCEL_OPTION);
			// if(result==JOptionPane.OK_OPTION){
			// passwd=passwordField.getText();
			// return true;
			// }
			// else{
			// return false;
			// }
		}

		public void showMessage(String message) {
			JOptionPane.showMessageDialog(null, message);
		}

		final GridBagConstraints gbc = new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.NORTHWEST,
				GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0);
		private Container panel;

		public String[] promptKeyboardInteractive(String destination, String name, String instruction, String[] prompt,
				boolean[] echo) {
			panel = new JPanel();
			panel.setLayout(new GridBagLayout());

			gbc.weightx = 1.0;
			gbc.gridwidth = GridBagConstraints.REMAINDER;
			gbc.gridx = 0;
			panel.add(new JLabel(instruction), gbc);
			gbc.gridy++;

			gbc.gridwidth = GridBagConstraints.RELATIVE;

			JTextField[] texts = new JTextField[prompt.length];
			for (int i = 0; i < prompt.length; i++) {
				gbc.fill = GridBagConstraints.NONE;
				gbc.gridx = 0;
				gbc.weightx = 1;
				panel.add(new JLabel(prompt[i]), gbc);

				gbc.gridx = 1;
				gbc.fill = GridBagConstraints.HORIZONTAL;
				gbc.weighty = 1;
				if (echo[i]) {
					texts[i] = new JTextField(20);
				} else {
					texts[i] = new JPasswordField(20);
				}
				panel.add(texts[i], gbc);
				gbc.gridy++;
			}

			if (JOptionPane.showConfirmDialog(null, panel, destination + ": " + name, JOptionPane.OK_CANCEL_OPTION,
					JOptionPane.QUESTION_MESSAGE) == JOptionPane.OK_OPTION) {
				String[] response = new String[prompt.length];
				for (int i = 0; i < prompt.length; i++) {
					response[i] = texts[i].getText();
				}
				return response;
			} else {
				return null; // cancel
			}
		}
	}

	private static int countLines(String str) {
		String[] lines = str.split("\r\n|\r|\n");
		return lines.length;
	}
}