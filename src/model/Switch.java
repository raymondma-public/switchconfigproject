package model;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;


/*
 * The switch model
 */
public class Switch {
	private ArrayList<Port> ports = new ArrayList();
	private String ip = "";
	private Set<String> esrpPort = new HashSet() {
		@Override
		public boolean contains(Object arg0) {
			// TODO Auto-generated method stub
			for (String s : esrpPort) {
				// System.out.println("compare: "+s+" "+(String)arg0);
				if (s.trim().equals(((String) arg0).trim())) {
					return true;
				}
			}
			return false;
		}

	};
	private ArrayList<String> sharingPortsWithEsrp = new ArrayList(){
		@Override
		public boolean contains(Object arg0) {
			// TODO Auto-generated method stub
			for (String s : sharingPortsWithEsrp) {
				// System.out.println("compare: "+s+" "+(String)arg0);
				if (s.trim().equals(((String) arg0).trim())) {
					return true;
				}
			}
			return false;
		}

	};
	private ArrayList<String> sharingPortsWithoutEsrp = new ArrayList(){
		@Override
		public boolean contains(Object arg0) {
			// TODO Auto-generated method stub
			for (String s : sharingPortsWithoutEsrp) {
				// System.out.println("compare: "+s+" "+(String)arg0);
				if (s.trim().equals(((String) arg0).trim())) {
					return true;
				}
			}
			return false;
		}

	};

	public ArrayList<String> getSharingPortsWithoutEsrp() {
		return sharingPortsWithoutEsrp;
	}

	public void addSharingPortsWithoutEsrp(String sharingPortsWithoutEsrp) {
		this.sharingPortsWithoutEsrp.add(sharingPortsWithoutEsrp);
	}

	public ArrayList<String> getSharingPortsWithEsrp() {
		return sharingPortsWithEsrp;
	}

	public void addSharingPortsWithEsrp(String sharingPortsWithEsrp) {
		this.sharingPortsWithEsrp.add(sharingPortsWithEsrp);
	}

	public Switch(String ip) {
		this.ip = ip;
	}

	public void addPort(String portNumber, String status) {
		ports.add(new Port(portNumber, status));
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public ArrayList<Port> getPorts() {
		return ports;
	}

	public void setPorts(ArrayList<Port> ports) {
		this.ports = ports;
	}

	public void addEsrpPort(String esrpPort) {
		this.esrpPort.add(esrpPort);
	}

	public boolean containEsrpPort(String esrpPort) {

		return this.esrpPort.contains(esrpPort);
	}

	public int getEsrpRecordsNumber() {
		return esrpPort.size();
	}

	
	/*
	 * Check the port with no esrp, and return a list of port number as string
	 */
	public ArrayList<String> checkPortNoEsrp() {
		ArrayList<String> resultAList = new ArrayList();
		for (Port p : ports) {
			if (!esrpPort.contains(p.getPortNumber())) {
				resultAList.add(p.getPortNumber());
			}
		}
		System.out.println(Arrays.toString(resultAList.toArray()));
		return resultAList;
	}

	/*
	 * Show the port that have no ESRP
	 */
	public void displayPortNoEsrp() {
		ArrayList<String> noEsrps = checkPortNoEsrp();
		
		JTextArea textArea = new JTextArea(Arrays.toString(noEsrps.toArray()));
		JScrollPane scrollPane = new JScrollPane(textArea);
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		scrollPane.setPreferredSize(new Dimension(500, 100));
		// JOptionPane.showMessageDialog(null, scrollPane,
		// "dialog test with textarea",
		// JOptionPane.YES_NO_OPTION);

		JFrame demo = new JFrame(ip);
		demo.setSize(500, 100);
		demo.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		demo.getContentPane().add(BorderLayout.CENTER, scrollPane);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		demo.setLocation(dim.width / 2 - demo.getSize().width / 2, 0);
		demo.setVisible(true);
	}

	
	/*
	 * Get the ports with no esrp and not shared
	 */
	private ArrayList<String> getPortNoEsrpNoShared(){
		ArrayList<String> noEsrps=checkPortNoEsrp();
		ArrayList<String> pairedPorts=sharingPortToSharedPorts(this.sharingPortsWithEsrp);
		ArrayList<String> output =new ArrayList();
		
		for(String s1:noEsrps){
			if(!pairedPorts.contains(s1)){
				output.add(s1);
			}
		}
		
		return output;
				
	}
	
	
	/*
	 * Show the ports that have no ESRP and Sharing 
	 */
	public void displayPortNoEsrpAndSharing() {
		// TODO Auto-generated method stub
		ArrayList<String> portNoEsrpNoShared=getPortNoEsrpNoShared();
		JTextArea textArea = new JTextArea(Arrays.toString(portNoEsrpNoShared.toArray()));
		JScrollPane scrollPane = new JScrollPane(textArea);
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		scrollPane.setPreferredSize(new Dimension(500, 100));
		// JOptionPane.showMessageDialog(null, scrollPane,
		// "dialog test with textarea",
		// JOptionPane.YES_NO_OPTION);
		
		JFrame demo = new JFrame(ip);
		demo.setSize(500, 100);
		demo.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		demo.getContentPane().add(BorderLayout.CENTER, scrollPane);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		demo.setLocation(dim.width / 2 - demo.getSize().width / 2, 0);
		demo.setVisible(true);
	}

	/*
	 * Converting sharing ports to shared ports
	 * Just add 1 to the sharing port
	 * eg. port 2:4 usually share with 2:5
	 */
	private ArrayList<String> sharingPortToSharedPorts(ArrayList<String> sharingPorts) {
		ArrayList<String> output = new ArrayList();
		for (String s : sharingPorts) {
			if (s.contains(":")) {
				String twoParts[] = s.split(":");
				int portNumber = Integer.parseInt(twoParts[1]);
				int paredPortNumber = portNumber + 1;
				String paredPort = twoParts[0] + ":" + paredPortNumber;
				output.add(paredPort);

			} else {
				int portNumber = Integer.parseInt(s);
				int paredPortNumber = portNumber + 1;
				output.add(""+paredPortNumber);
			}

		}

		return output;
	}
}
