package model;

import java.io.Serializable;

public class UserPreference implements Serializable {
	boolean showAllFrame=false;
	boolean storeUserNamePw=false;
	public UserPreference() {
		super();

	}


	public UserPreference(boolean showAllFrame, boolean storeUserNamePw) {
		super();
		this.showAllFrame = showAllFrame;
		this.storeUserNamePw = storeUserNamePw;
	}


	public boolean isShowAllFrame() {
		return showAllFrame;
	}

	public void setShowAllFrame(boolean showAllFrame) {
		this.showAllFrame = showAllFrame;
	}
	public boolean isStoreUserNamePw() {
		return storeUserNamePw;
	}
	public void setStoreUserNamePw(boolean storeUserNamePw) {
		this.storeUserNamePw = storeUserNamePw;
	}
	
	
}
