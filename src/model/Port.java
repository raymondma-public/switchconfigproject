package model;

/*
 * Model for storing the port and its status
 */
public class Port {
	private String portNumber="";
	private String status="";
	public Port (String portNumber,String status ){
		this.portNumber=portNumber;
		this.status=status;
		
	}
	public String getPortNumber() {
		return portNumber;
	}
	public void setPortNumber(String portNumber) {
		this.portNumber = portNumber;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
}
