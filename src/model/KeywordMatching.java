package model;

import java.util.ArrayList;
import java.util.Arrays;

public class KeywordMatching extends MatchingPattern {

	private ArrayList<String > keywords=new ArrayList();
	
	public KeywordMatching(String name, String pattern) {
		super(name, pattern);
		String keywordsArr[]=pattern.split(" ");
		keywords.addAll(Arrays.asList(keywordsArr));
	}

	public ArrayList<String> getKeywords() {
		return keywords;
	}

	public void setKeywords(ArrayList<String> keywords) {
		this.keywords = keywords;
	}

	
}
