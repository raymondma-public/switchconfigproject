package model;

import java.io.Serializable;

public class User implements Serializable{
	String username;
	String password;
	UserPreference preference=new UserPreference();
	
	public User(String username, String password,UserPreference preference) {
		super();
		this.username = username;
		this.password = password;
		this.preference=preference;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public void setShowAllFrame(boolean showAllFrame){
		preference.setShowAllFrame(showAllFrame);
	}
	
	public boolean isShowAllFrame(){
		return preference.isShowAllFrame();
	}
	
	public void setStoreUsrPassword(boolean storeUserNamePw){
		preference.setStoreUserNamePw(storeUserNamePw);;
	}
	
	
	public boolean isStoreUserPassword(){
		return preference.isStoreUserNamePw();
	}
}
