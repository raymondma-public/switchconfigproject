package model;

/*
 * Model for storing the loopbackip, vlan name, ip, subnetmask
 */
public class IPMapping {
	private String loopbackIp;
	private String vlanName;
	private String ip;
	private String subnetMask;
	
	
	
	public String getVlanName() {
		return vlanName;
	}

	public void setVlanName(String vlanName) {
		this.vlanName = vlanName;
	}

	public IPMapping(String loopbackIp, String vlanName, String ip, String subnetMask) {
		super();
		this.loopbackIp = loopbackIp;
		this.vlanName = vlanName;
		this.ip = ip;
		this.subnetMask = subnetMask;
	}

	public IPMapping() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getLoopbackIp() {
		return loopbackIp;
	}
	public void setLoopbackIp(String loopbackIp) {
		this.loopbackIp = loopbackIp;
	}
	public String getIp() {
		return this.ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getSubnetMask() {
		return subnetMask;
	}
	public void setSubnetMask(String subnetMask) {
		this.subnetMask = subnetMask;
	}
	
}
