package util;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/*
 * Handle the file and folder
 */
public class FileAndFolderTool {

	
	public static final String  OLD_SWITCH_FOLDER="OLD_SWITCH_FOLDER";
	public static final String NEW_SWITCH_FOLDER="NEW_SWITCH_FOLDER";
	public static final String OTHER_FOLDER="OTHER_FOLDER";
	/*
	 * This create the and write file 
	 * Input: ip, command , content
	 * Result: create the txt file named <command>-<ip>.txt with content
	 */
	public static void createAndWriteFile(String ip, String command, String content) {
		content = content.replace("\n", "\r\n");
		String outputString = "";
		System.out.println();
		// Scanner scanner = new Scanner(allString);

		String folderName = getCurrentFolderName();// date as name
		createFolderIfNotExist(".", folderName);
		createFolderIfNotExist(folderName, ip);
		createFolderIfNotExist(folderName, command);

		try {
			PrintWriter writer = new PrintWriter("./" + folderName + "/" + ip + "/" + command + " - " + ip + ".txt",
					"UTF-8");

			writer.println(content);

			writer.close();
		} catch (Exception e) {
			// do something
		}

		try {
			PrintWriter writer = new PrintWriter(
					"./" + folderName + "/" + command + "/" + command + " - " + ip + ".txt", "UTF-8");

			writer.println(content);

			writer.close();
		} catch (Exception e) {
			// do something
		}
		// scanner.close();
	}

	
	/*
	 * Create and write the IP Mapping file
	 * loopbackip will be the name of file
	 */
	public static void createAndWriteIPMapFile(String loopbackIp, String content) {
		content = content.replace("\n", "\r\n");
		String outputString = "";
		System.out.println();
		// Scanner scanner = new Scanner(allString);

		String folderName = getCurrentFolderName();// date as name
		createFolderIfNotExist(".", folderName);

		try {
			PrintWriter writer = new PrintWriter("./" + folderName + "/" + loopbackIp + ".txt",
					"UTF-8");

			writer.println(content);

			writer.close();
		} catch (Exception e) {
			// do something
		}


	}
	
	/*
	 * Input: parent folder name, folder name wanted
	 * Result: generate folder with your folder name if it was not exist 
	 */
	
	private static void createFolderIfNotExist(String parent, String folderName) {
		File theDir = new File(parent + "/" + folderName);

		// if the directory does not exist, create it
		if (!theDir.exists()) {
			System.out.println("creating directory: " + folderName);
			boolean result = false;

			try {
				theDir.mkdir();
				result = true;
			} catch (SecurityException se) {
				// handle it
			}
			if (result) {
				System.out.println("DIR created");
			}
		}
	}

	
	/*
	 * Return the folder name with today date
	 */
	public static String getCurrentFolderName() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd");
		// get current date time with Date()
		Date date = new Date();
		String folderName = dateFormat.format(date);
		return folderName;
	}

	/*
	 * Get all string from the file
	 */
	public static String readFile(String path, Charset encoding) throws IOException {
		byte[] encoded = Files.readAllBytes(Paths.get(path));
		return new String(encoded, encoding);
	}

	
}
