package util;

import java.util.Scanner;

import model.KeywordMatching;
import model.MatchingPattern;

public class PatternMatchinUtil {

	private static PatternMatchinUtil instance = new PatternMatchinUtil();

	private PatternMatchinUtil() {

	}

	public static PatternMatchinUtil getInstance() {
		return instance;
	}

	public String getStringMatchedKeywords(String wholeString, KeywordMatching keywordMatching) {
		Scanner wholeStringScanner = new Scanner(wholeString);
		String resultString="";
		while (wholeStringScanner.hasNextLine()) {// Every line (Every
													// Command)
			String currentLine=wholeStringScanner.nextLine();
			boolean match=true;
			for (String keyword : keywordMatching.getKeywords()) {
				if(currentLine.contains(keyword)){
					
				}else{//no contain keyword
					match=false;
					break;
				}
				
			}
			
			
			if(match){
				resultString+=currentLine+"\n";
			}
		}
		return resultString;
	}

	public String getStringMatched(String wholeString, MatchingPattern matchingPattern) {
		String resultString = "";

		String matchingPatternSplit[] = matchingPattern.getPattern().split(" ");

		Scanner wholeStringScanner = new Scanner(wholeString);

		while (wholeStringScanner.hasNextLine()) {// Every line (Every Command)

			String currentLine = wholeStringScanner.nextLine();
			String currentLineSplit[] = currentLine.split(" ");

			String currentLineResult = "";
			if (matchingPatternSplit.length == currentLineSplit.length) {

				for (int i = 0; i < matchingPatternSplit.length; i++) {// Every
																		// words
																		// in
																		// command

					if (matchingPatternSplit[i].equals(currentLineSplit[i])) {// one
																				// is
																				// match

					} else if (matchingPatternSplit[i].equals("<?>")) {// is
																		// what
																		// we
																		// want
						currentLineResult += currentLineSplit[i] + "\t";
					} else {// contant something we don't want/don't match
						currentLineResult = "";
						break;
					}

					if (i == matchingPatternSplit.length - 1) {// last word(must
																// be
																// match/wanted
																// if reach
																// here)
						resultString += currentLineResult + "\n";
					}
				}

			}

		} // end While

		return resultString;

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String wholeString = "configure vlan V252_96 ipaddress 160.30.252.98 255.255.255.248";
		String pattern = "configure vlan <?> ipaddress <?> <?>";
		MatchingPattern matchingPattern = new MatchingPattern("name", pattern);
		String result = PatternMatchinUtil.getInstance().getStringMatched(wholeString, matchingPattern);
		System.out.println(result);

	}

}
