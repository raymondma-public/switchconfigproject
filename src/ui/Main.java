package ui;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import exec.Exec;
import getip.GetIPUtil;
import model.IPMapping;
import model.KeywordMatching;
import model.MatchingPattern;
import util.FileAndFolderTool;
import util.PatternMatchinUtil;

/*
 * This class is the starting point of the whole Project
 */
public class Main {

	private static ArrayList<File> selectedFiles = new ArrayList();// A list of
																	// file from
																	// the
																	// folder
																	// selected
	// private static ArrayList<File> selectedCfgFiles = new ArrayList();

	private static String summary = "";// The string for storing all the result
										// from all switches
	private static String keywordSummary = "";// The string for storing all the
												// result

	private static String targetFileType;
	// from all switches

	public static void main(String[] args) {

		JFrame.setDefaultLookAndFeelDecorated(true);
		JDialog.setDefaultLookAndFeelDecorated(true);
		JFrame frame = new JFrame("JComboBox Test");
		frame.setBounds(0, 0, 300, 300);
		frame.setLayout(new FlowLayout());
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JButton keywordsBtn = new JButton("Search with keywords");

		keywordsBtn.addActionListener(new ActionListener() {

			private void processSelectedFilesWithKeywords(KeywordMatching keywordMatching) {
				for (File selectedFile : selectedFiles) {
					System.out.println(selectedFile.getAbsolutePath());
					try {

						String wholeFileString = FileAndFolderTool.readFile(selectedFile.getAbsolutePath(),
								StandardCharsets.UTF_8);
						System.out.println(wholeFileString);

						ArrayList<IPMapping> ipMapList = GetIPUtil.getIPMapFromConfig(wholeFileString);

						String allIpMapString = PatternMatchinUtil.getInstance()
								.getStringMatchedKeywords(wholeFileString, keywordMatching);
						String loopbackIp ="";
						if(targetFileType.equalsIgnoreCase(FileAndFolderTool.OLD_SWITCH_FOLDER)){
							 loopbackIp = ipMapList.get(0).getLoopbackIp();
						}else if(targetFileType.equalsIgnoreCase(FileAndFolderTool.NEW_SWITCH_FOLDER)){
							loopbackIp = findIpFromCFG(wholeFileString);
						}
						allIpMapString = Main.addFieldToEveryLineFront(loopbackIp, allIpMapString);
						allIpMapString = Main.addFieldToEveryLineFront(keywordMatching.getName(), allIpMapString);

						keywordSummary += allIpMapString;
						FileAndFolderTool.createAndWriteIPMapFile(loopbackIp + "_" + keywordMatching.getName(),
								allIpMapString);

					} catch (IOException e) {
						e.printStackTrace();
					}
				}

			}// End method

			private String findIpFromCFG(String wholeFileString) {
				// TODO Auto-generated method stub
				Scanner scanner=new Scanner(wholeFileString);
				boolean loopbackModeStarted=false;
				String resultString="";
				while(scanner.hasNextLine()){
					String currentLine=scanner.nextLine();
					
					if(currentLine.trim().equalsIgnoreCase("interface loopback0".trim())){
						loopbackModeStarted=true;
					}
					
					if(loopbackModeStarted && currentLine.toLowerCase().trim().contains("ip address".trim().toLowerCase())){
						JOptionPane.showMessageDialog(null, currentLine);
						int ipStartPos=currentLine.indexOf("ip address")+"ip address".length();
						String wholeIpString=currentLine.substring(ipStartPos).trim();
						
						String removedSpace=wholeIpString.split(" ")[0].trim();
						JOptionPane.showMessageDialog(null, removedSpace);
						String removedSlash=removedSpace.split("/")[0].trim();
						
						return resultString=removedSlash;
					}
				}
				
				
				return resultString;
			}

			@Override
			public void actionPerformed(ActionEvent e) {
				keywordSummary = "";
				// TODO Auto-generated method stub
				Scanner fileScanner;

				ArrayList<KeywordMatching> keywordList = new ArrayList();
				try {
					fileScanner = new Scanner(new File("keywords.txt"));

					while (fileScanner.hasNextLine()) {
						// ArrayList<KeywordMatching > keywordList=new
						// ArrayList();
						String wholeKeywordFileLine = fileScanner.nextLine();

						String wholeKeywordFileLineSplit[] = wholeKeywordFileLine.split("<split>");

						String keywordName = wholeKeywordFileLineSplit[0].trim();
						String keywords = wholeKeywordFileLineSplit[1].trim();
						// String keywordSplit[]=keywords.split(" ");

						keywordList.add(new KeywordMatching(keywordName, keywords));

					}

					
					final JPanel panel = new JPanel();
					final JRadioButton xsfRBtn = new JRadioButton("XSF");
					final JRadioButton cfgRBtn = new JRadioButton("CFG");

					panel.add(xsfRBtn);
					panel.add(cfgRBtn);

					JOptionPane.showMessageDialog(null, panel);

					if (xsfRBtn.isSelected()) {
						targetFileType = FileAndFolderTool.OLD_SWITCH_FOLDER;
					} else if (cfgRBtn.isSelected()) {
						targetFileType = FileAndFolderTool.NEW_SWITCH_FOLDER;
					}
					
					JFileChooser fileChooser = new JFileChooser("./");// Create
					// file
					// chooser
					// from
					// the
					// program
					// directory
					fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
					
					

					int returnValue = fileChooser.showOpenDialog(null);
					if (returnValue == JFileChooser.APPROVE_OPTION) {

						for (KeywordMatching keywordMatching : keywordList) {
							File folder = fileChooser.getSelectedFile();
							selectedFiles.clear();
							// selectedCfgFiles.clear();

							listFilesForFolder(folder);

							processSelectedFilesWithKeywords(keywordMatching);

							FileAndFolderTool.createAndWriteIPMapFile("KeywordSummary", keywordSummary);

						}

					}

				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}

		}

		);
		frame.add(keywordsBtn);

		JButton button = new JButton("Get Ip From Config(s)");

		/*
		 * Adding action listener to button for performing action Extrating .xsf
		 * file from the selected folder to seperated files and one summary
		 */
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				summary = "";
				Scanner fileScanner;
				try {
					fileScanner = new Scanner(new File("pattern.txt"));

					ArrayList<MatchingPattern> matchingPatternList = new ArrayList();
					while (fileScanner.hasNextLine()) {
						String wholePatternFileLine = fileScanner.nextLine();
						String[] wholePatternFileLineSplit = wholePatternFileLine.split("<split>");
						String currentPattern = wholePatternFileLineSplit[0].trim();
						String patternName = wholePatternFileLineSplit[1].trim();
						// JOptionPane.showMessageDialog(null, currentPattern);
						matchingPatternList.add(new MatchingPattern(patternName, currentPattern));
					}

					final JPanel panel = new JPanel();
					final JRadioButton xsfRBtn = new JRadioButton("XSF");
					final JRadioButton cfgRBtn = new JRadioButton("CFG");

					panel.add(xsfRBtn);
					panel.add(cfgRBtn);

					JOptionPane.showMessageDialog(null, panel);

					if (xsfRBtn.isSelected()) {
						targetFileType = FileAndFolderTool.OLD_SWITCH_FOLDER;
					} else if (cfgRBtn.isSelected()) {
						targetFileType = FileAndFolderTool.NEW_SWITCH_FOLDER;
						JOptionPane.showMessageDialog(null, "Not Implemented Function");
						return;
					}
					
					JFileChooser fileChooser = new JFileChooser("./");// Create
																		// file
																		// chooser
																		// from
																		// the
																		// program
																		// directory
					fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
					int returnValue = fileChooser.showOpenDialog(null);
					if (returnValue == JFileChooser.APPROVE_OPTION) {

						for (MatchingPattern matchingPattern : matchingPatternList) {
							File folder = fileChooser.getSelectedFile();
							selectedFiles.clear();
							listFilesForFolder(folder);

							processSelectedFiles(selectedFiles, matchingPattern);

							FileAndFolderTool.createAndWriteIPMapFile("Summary", summary);

						}

					}
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			/*
			 * Input: list of selected files(expedted .xsf file) Result: Wanted
			 * ip will be extracted
			 */
			private void processSelectedFiles(ArrayList<File> selectedFiles, MatchingPattern matchingPattern) {
				for (File selectedFile : selectedFiles) {

					System.out.println(selectedFile.getAbsolutePath());
					if (getExtension(selectedFile.getAbsolutePath()).equalsIgnoreCase("xsf")) {

						processXsfFile(matchingPattern, selectedFile);

					} else if (getExtension(selectedFile.getAbsolutePath()).equalsIgnoreCase("cfg")) {
						
						

					}

				}
			}// End method

			private void processXsfFile(MatchingPattern matchingPattern, File selectedFile) {
				try {
					
					String wholeFileString = FileAndFolderTool.readFile(selectedFile.getAbsolutePath(),
							StandardCharsets.UTF_8);
					System.out.println(wholeFileString);

					ArrayList<IPMapping> ipMapList = GetIPUtil.getIPMapFromConfig(wholeFileString);

					/*
					 * 
					 * 
					 * String allIpMapString = ""; for (IPMapping ipMap :
					 * ipMapList) { allIpMapString += ipMap.getLoopbackIp() +
					 * "\t" + ipMap.getVlanName() + "\t" + ipMap.getIp() + "\t"
					 * + ipMap.getSubnetMask() + "\n"; }
					 */

					String allIpMapString = PatternMatchinUtil.getInstance().getStringMatched(wholeFileString,
							matchingPattern);

					String loopbackIp = ipMapList.get(0).getLoopbackIp();
					// summary +="\n\nLoopback IP:\t"+loopbackIp+"\n";
					// summary +="Pattern
					// Name:\t"+matchingPattern.getName()+"\n";
					// summary
					// +="Pattern:\t"+matchingPattern.getPattern()+"\n";
					
					allIpMapString = Main.addFieldToEveryLineFront(loopbackIp, allIpMapString);
					allIpMapString = Main.addFieldToEveryLineFront(matchingPattern.getName(), allIpMapString);

					summary += allIpMapString;

					FileAndFolderTool.createAndWriteIPMapFile(loopbackIp + "_" + matchingPattern.getName(),
							allIpMapString);

				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
		frame.add(button);

		/*
		 * This button is for getting configuration from remote switches
		 * 
		 */
		JButton getConfigButton = new JButton("Get Config");
		getConfigButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				Exec.main(null);
			}

		});

		frame.add(getConfigButton);
		frame.pack();
		frame.setVisible(true);
	}

	protected static String addFieldToEveryLineFront(String frontString, String allIpMapString) {
		String resultString = "";
		Scanner scanner = new Scanner(allIpMapString);
		while (scanner.hasNextLine()) {
			String currentLine = scanner.nextLine();
			resultString += frontString + "\t" + currentLine + "\n";

		}
		return resultString;
	}

	/*
	 * This method is for listing all files in the folder and add the wanted
	 * files to the selected file list
	 */
	public static void listFilesForFolder(final File folder) {

		for (final File fileEntry : folder.listFiles()) {// for all things in
															// folder

			if (fileEntry.isDirectory()) {// directory
				listFilesForFolder(fileEntry);

			} else {// files

				if (switchFolderType(folder).equalsIgnoreCase(targetFileType)) {// if
																				// file
																				// type
																				// match

					System.out.println(fileEntry.getName());
					selectedFiles.add(new File(fileEntry.getAbsolutePath()));

				}

				// else
				// if(switchFolderType(folder).equalsIgnoreCase(FileAndFolderTool.NEW_SWITCH_FOLDER)){
				// selectedFiles.add(new File(fileEntry.getAbsolutePath()));
				//// selectedCfgFiles.add(new
				// File(fileEntry.getAbsolutePath()));
				// }

			}
		}
	}

	private static String switchFolderType(File folder) {
		boolean haveXSF = false;
		boolean haveCFG = false;

		for (final File fileEntry : folder.listFiles()) {// for all things in
															// folder
			if (getExtension(fileEntry.getAbsolutePath()).equalsIgnoreCase("xsf")) {
				haveXSF = true;
			} else if (getExtension(fileEntry.getAbsolutePath()).equalsIgnoreCase("cfg")) {
				haveCFG = true;
			}
		}

		if ((haveXSF && haveCFG) || haveXSF) {
			return FileAndFolderTool.OLD_SWITCH_FOLDER;
		} else if (haveCFG && !haveXSF) {
			return FileAndFolderTool.NEW_SWITCH_FOLDER;
		} else {
			return FileAndFolderTool.OTHER_FOLDER;
		}

	}

	private static boolean matchExtension(File fileEntry) {
		return getExtension(fileEntry.getAbsolutePath()).equals("xsf");// ||getExtension(fileEntry.getAbsolutePath()).equals("cfg");

	}

	/*
	 * Getting the file extension with fileName
	 */
	public static String getExtension(String fileName) {
		String extension = "";

		int i = fileName.lastIndexOf('.');
		if (i > 0) {
			extension = fileName.substring(i + 1);
		}
		return extension;
	}
}
