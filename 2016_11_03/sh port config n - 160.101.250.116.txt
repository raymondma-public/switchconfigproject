Port Configuration
Port     Virtual    Port  Link  Auto   Speed      Duplex   Flow  Load   Media
         router     State State Neg  Cfg Actual Cfg Actual Cntrl Master Pri Red
================================================================================
1        None        E      A    ON  AUTO  1000 AUTO FULL    SYM        LX      
2        None        E      R    ON  AUTO       AUTO                    NONE    
3        None        E      A    ON  AUTO  1000 AUTO FULL   NONE        SX      
4        None        E      A    ON  AUTO  1000 AUTO FULL   NONE        SX      
5        None        E      A    ON  AUTO  1000 AUTO FULL   NONE        BASET   
6        None        E      R    ON  AUTO       AUTO                    NONE    
7        None        E      A    ON  AUTO  1000 AUTO FULL   NONE        SX      
8        None        E      A    ON  AUTO  1000 AUTO FULL   NONE        SX      
9        None        E      R    ON  AUTO       AUTO                    NONE    
10       None        E      A    ON  AUTO  1000 AUTO FULL    SYM        SX      
11       None        E      R    ON  AUTO       AUTO                    NONE    
12       None        E      R    ON  AUTO       AUTO                    NONE    
13       None        E      R    ON  AUTO       AUTO                    NONE    
14       None        E      R    ON  AUTO       AUTO                    NONE    
15       None        E      R    ON  AUTO       AUTO                    NONE    
16       None        E      R    ON  AUTO       AUTO                    NONE    
17       None        E      R    ON  AUTO       AUTO                    NONE    
18       None        E      R    ON  AUTO       AUTO                    NONE    
19       None        E      R    ON  AUTO       AUTO                    NONE    
20       None        E      R    ON  AUTO       AUTO                    NONE    
21       None        E      R    ON  AUTO       AUTO                    NONE   *
22       None        E      R    ON  AUTO       AUTO                    NONE   *
23       None        E      A    ON  AUTO  1000 AUTO FULL    SYM    23  SX  UTP 
24       None        E      A    ON  AUTO  1000 AUTO FULL    SYM    23  SX  UTP 
25       None        E      R    ON  AUTO       AUTO                    UTP     
26       None        E      R    ON  AUTO       AUTO                    UTP     
27       None        E      R    ON  AUTO       AUTO                    UTP     
28       None        E      R    ON  AUTO       AUTO                    UTP     
29       VR-Default  E     NP   OFF 10000       FULL                    NONE    
30       VR-Default  E     NP   OFF 10000       FULL                    NONE    
31       VR-Default  E     NP   OFF 10000       FULL                    NONE    
32       VR-Default  E     NP   OFF 10000       FULL                    NONE    
33       VR-Default  E     NP   OFF 10000       FULL                    NONE    
34       VR-Default  E     NP   OFF 10000       FULL                    NONE    
================================================================================
          > indicates Port Display Name truncated past 8 characters
          Link State: A-Active R-Ready NP- Port not present L-Loopback 
          Port State: D-Disabled, E-Enabled
          Media: !-Unsupported Optic Module
          Media Red: * - use "show port info detail" for redundant media type
          Flow Cntrl: Shows link partner's abilities. NONE if Auto Neg is OFF

