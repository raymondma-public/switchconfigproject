Port Configuration
Port     Virtual    Port  Link  Auto   Speed      Duplex   Flow  Load   Media
         router     State State Neg  Cfg Actual Cfg Actual Cntrl Master Pri Red
================================================================================
1        VR-Default  E      A   OFF   100   100 FULL FULL   NONE        BASET   
2        VR-Default  E      R    ON  AUTO       AUTO                    NONE    
3        VR-Default  E      A    ON  AUTO  1000 AUTO FULL    SYM        BASET   
4        VR-Default  E      R    ON  AUTO       AUTO                    NONE    
5        VR-Default  E      A    ON  AUTO  1000 AUTO FULL    SYM        BASET   
6        VR-Default  E      R    ON  AUTO       AUTO                    NONE    
7        VR-Default  E      R    ON  AUTO       AUTO                    NONE    
8        VR-Default  E      R    ON  AUTO       AUTO                    NONE    
9        VR-Default  E      R    ON  AUTO       AUTO                    NONE    
10       VR-Default  E      R    ON  AUTO       AUTO                    NONE    
11       VR-Default  E      R    ON  AUTO       AUTO                    NONE    
12       VR-Default  E      R    ON  AUTO       AUTO                    NONE    
13       VR-Default  E      R    ON  AUTO       AUTO                    NONE    
14       VR-Default  E      R    ON  AUTO       AUTO                    NONE    
15       VR-Default  E      R    ON  AUTO       AUTO                    NONE    
16       VR-Default  E      R    ON  AUTO       AUTO                    NONE    
17       VR-Default  E      R    ON  AUTO       AUTO                    NONE    
18       VR-Default  E      R    ON  AUTO       AUTO                    NONE    
19       VR-Default  E      A    ON  AUTO  1000 AUTO FULL   NONE        SX      
20       VR-Default  E      R    ON  AUTO       AUTO                    BASET   
21       VR-Default  E      R    ON  AUTO       AUTO                    NONE   *
22       VR-Default  E      A    ON  AUTO  1000 AUTO FULL SY/ASY       UTP     *
23       VR-Default  E      A    ON  AUTO  1000 AUTO FULL    SYM    23  SX  UTP 
24       VR-Default  E      A    ON  AUTO  1000 AUTO FULL    SYM    23  SX  UTP 
25       VR-Default  E      A   OFF   100   100 FULL FULL   NONE        UTP     
26       VR-Default  E      R    ON  AUTO       AUTO                    UTP     
27       VR-Default  E      R    ON  AUTO       AUTO                    UTP     
28       VR-Default  E      R    ON  AUTO       AUTO                    UTP     
29       VR-Default  E     NP   OFF 10000       FULL                    NONE    
30       VR-Default  E     NP   OFF 10000       FULL                    NONE    
31       VR-Default  E     NP   OFF 10000       FULL                    NONE    
32       VR-Default  E     NP   OFF 10000       FULL                    NONE    
33       VR-Default  E     NP   OFF 10000       FULL                    NONE    
34       VR-Default  E     NP   OFF 10000       FULL                    NONE    
================================================================================
          > indicates Port Display Name truncated past 8 characters
          Link State: A-Active R-Ready NP- Port not present L-Loopback 
          Port State: D-Disabled, E-Enabled
          Media: !-Unsupported Optic Module
          Media Red: * - use "show port info detail" for redundant media type
          Flow Cntrl: Shows link partner's abilities. NONE if Auto Neg is OFF

