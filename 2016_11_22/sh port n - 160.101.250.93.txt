Port Summary
Port  Display         VLAN Name          Port  Link  Speed  Duplex
#     String          (or # VLANs)       State State Actual Actual
==================================================================
1                     (0004)              E     A     1000  FULL 
2                                         E     R                            
3                     v230_246            E     A     1000  FULL 
4                                         E     R                            
5                     v230_254            E     A     1000  FULL 
6                     (0004)              E     A     1000  FULL 
7                     (0004)              E     R                            
8                     (0004)              E     R                            
9                     (0004)              E     A     1000  FULL 
10                    (0004)              E     R                            
11                                        E     R                            
12                                        E     R                            
13                                        E     R                            
14                                        E     R                            
15                                        E     R                            
16                                        E     R                            
17                                        E     R                            
18                                        E     R                            
19                                        E     R                            
20                                        E     R                            
21                                        E     R                            
22                                        E     R                            
23                    (0008)              E     A     1000  FULL 
24                    (0008)              E     A     1000  FULL 
25                                        E     R                            
26                                        E     R                            
27                                        E     R                            
28                                        E     R                            
29                                        E     NP                           
30                                        E     NP                           
==================================================================
   Port State: D-Disabled, E-Enabled
   Link State: A-Active, R-Ready, NP-Port not present, L-Loopback,
               D-ELSM enabled but not up
               d-Ethernet OAM enabled but not up

