Port Summary
Port  Display         VLAN Name          Port  Link  Speed  Duplex
#     String          (or # VLANs)       State State Actual Actual
==================================================================
1                     (0007)              E     A     1000  FULL 
2                                         E     R                            
3                     (0008)              E     A     1000  FULL 
4                     (0008)              E     A     1000  FULL 
5                     (0009)              E     A     1000  FULL 
6                     (0009)              E     R                            
7                     (0009)              E     A     1000  FULL 
8                     (0008)              E     A     1000  FULL 
9                                         E     R                            
10                    V230_238            E     A     1000  FULL 
11                                        E     R                            
12                                        E     R                            
13                                        E     R                            
14                                        E     R                            
15                                        E     R                            
16                                        E     R                            
17                                        E     R                            
18                                        E     R                            
19                                        E     R                            
20                                        E     R                            
21                                        E     R                            
22                                        E     R                            
23                    (0016)              E     A     1000  FULL 
24                    (0016)              E     A     1000  FULL 
25                                        E     R                            
26                                        E     R                            
27                                        E     R                            
28                                        E     R                            
29                                        E     NP                           
30                                        E     NP                           
31                                        E     NP                           
32                                        E     NP                           
33                                        E     NP                           
34                                        E     NP                           
==================================================================
   Port State: D-Disabled, E-Enabled
   Link State: A-Active, R-Ready, NP-Port not present, L-Loopback,
               D-ELSM enabled but not up
               d-Ethernet OAM enabled but not up


